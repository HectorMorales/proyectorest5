<?php
require '../../app/vendor/autoload.php';
$app = new \Slim\Slim();
$app->config(array(
    'templates.path' => '../../app/templates/'
));
$app->container->singleton('db', function () {
  return new PDO("pgsql:host=10.0.2.15 port=5432 user=app_usuario password=12345 dbname=sociodemografica");
});

require '../../app/routes/root.php';
require '../../app/routes/response.php';

$app->run();
