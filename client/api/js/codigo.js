/*-----------------------------------------------------------------------------------*/  
function asociar_hijo(obj,padre){
        padre.appendChild(obj);
        return padre;
    }
/*-----------------------------------------------------------------------------------*/  
function crear_div(atributos){
   var obj_div= document.createElement("div");
    for(var i=0; i< atributos.length;i++){
      obj_div.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
     return obj_div;
}
 
atributos_container=[];
atributos_container[0]={"atributo":"class","valor":"container"};
atributos_row=[];
atributos_row[0]={"atributo":"class","valor":"row"};
atributos_esp=[];
atributos_esp[0]={"atributo":"class","valor":"col-md-1"};
atributos_entidad=[];
atributos_entidad[0]={"atributo":"class","valor":"col-md-2"};
atributos_municipio=[];
atributos_municipio[0]={"atributo":"class","valor":"col-md-2"};
atributos_tema=[];
atributos_tema[0]={"atributo":"class","valor":"col-md-2"};
atributos_indicador=[];
atributos_indicador[0]={"atributo":"class","valor":"col-md-3"};
atributos_pcentrado=[];
atributos_pcentrado[0]={"atributo":"class","valor":"text-center"};

atributos_grafica=[];
atributos_grafica[0]={"atributo":"id","valor":"graficaLineal"};
atributos_grafica[1]={"atributo":"class","valor":"col-md-11"};
/*-----------------------------------------------------------------------------------*/  
function crear_boton(atributos,_id,texto){
    atributos[atributos.length]={"atributo":"id","valor":_id};
   var obj_boton = document.createElement("button");
    for(var i=0; i< atributos.length;i++){
      obj_boton.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
	 obj_boton.textContent = texto;
      return obj_boton;
    }

boton_atributos=[];
boton_atributos[0]={"atributo":"type","valor":"button"};
boton_atributos[1]={"atributo":"class","valor":"btn btn-primary"};
/*-----------------------------------------------------------------------------------*/  
function add_func_boton(_boton,nom_funcion){
    _boton.addEventListener('click', nom_funcion,false);
    return _boton;
}

/*-----------------------------------------------------------------------------------*/  
function crear_select(atributos,_id,opciones){
   atributos[atributos.length]={"atributo":"id","valor":_id};
   var obj_select = document.createElement("select");
        for(var i=0; i< atributos.length;i++){
      obj_select.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
     for(var j=0; j < opciones.length; j++){
       obj_opcion=document.createElement("option");
       obj_opcion.textContent= opciones[j];
       obj_select.appendChild(obj_opcion);
     }
       return obj_select;
}

atributos_select=[];
atributos_select[0]={"atributo":"class","valor":"form-control"}
//opciones_select=["Nacional","Oaxaca"];

function crear_select_on(atributos,_id,opciones,funcion){
    funcion=funcion+"()";
   atributos[atributos.length]={"atributo":"id","valor":_id};
   atributos[atributos.length]={"atributo":"onclick","valor":funcion};
   var obj_select = document.createElement("select");
        for(var i=0; i< atributos.length;i++){
      obj_select.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
     for(var j=0; j < opciones.length; j++){
       obj_opcion=document.createElement("option");
       obj_opcion.textContent= opciones[j];
       obj_select.appendChild(obj_opcion);
     }
       return obj_select;
}
/*-----------------------------------------------------------------------------------*/  
//--------funcion que realiza el GET al servidor ---------------//
function solicitar(recurso) {
   var servidor = "http://hpw.economia.nacional/";
    var url = servidor +"api/"+recurso;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
          respuesta = xhr.responseText;
        if (this.status === 200) {
              console.log("recivido");
        } 
        else if (this.status === 400) {
             console.log("error 404");
        } 
        else if (this.status === 404) {
           console.log("error 404");
       } else {
        var data ={"mensaje":"Error:"+xhr.status};
           return data;
        }
      }
      
    };
    xhr.open('GET', url);
    xhr.setRequestHeader("accept", "aplication/json");
    xhr.send();
    
    var data=JSON.parse(respuesta);
    return data;
 }
 
 //------ seccion de datos ---//
 
   function leer_indicador(){
  var desc_indicador= document.getElementById("indicadores").value
  
  for (var llave in obj_indicador.indicadores) {
      var comp_indicador= obj_indicador.indicadores[llave].indicador.desc_indicador;
      if( comp_indicador == desc_indicador){
          var id_indicador=obj_indicador.indicadores[llave].indicador.id;
     }
     }
      return id_indicador;
}

 function solicitar_datos(){
    var recurso = "entidades/"+leer_entidad()+"/municipios/"+leer_municipio()+"/temas/"+leer_tema()+"/indicadores/"+leer_indicador();
    solicitar(recurso);
}
 
 function inicia_datos (){
     solicitar_datos();
   
     if (document.getElementById("graficar")){
           div_boton.removeChild(b_graficar);
     };

b_graficar= crear_boton(boton_atributos,"graficar","Graficar");
add_func_boton(b_graficar,get_datos);
asociar_hijo(b_graficar,div_boton);
 
     
 }
 
 function get_datos(){
div_grafica.style.display = 'inline';

      load_datos(leer_entidad(),leer_municipio(),leer_tema(),leer_indicador());

 }
 
 
 function load_datos(_id_entidad,_id_municicpio,_id_tema,_id_indicador){
   var  recurso="entidades/"+_id_entidad+"/municipios/"+_id_municicpio+"/temas/"+_id_tema+"/indicadores/"+_id_indicador;
  console.log(recurso);
  data_indicador=solicitar(recurso);
gui_datos(data_indicador);
}

function gui_datos(data_indicador) {
arr_anios=[]
for (var i=0; i< data_indicador.indicador.anios.length; i++) {
    arr_anios[i]= data_indicador.indicador.anios[i].periodo.anio;
}
arr_valor=[]
for (var i=0; i< data_indicador.indicador.anios.length; i++) {
    arr_valor[i]= data_indicador.indicador.anios[i].periodo.valor;
}
	var chart;

		chart = new Highcharts.Chart({
			chart: {
				type: 'column',
			renderTo: 'graficaLineal' 	},
			title: {
				text: data_indicador.indicador.desc_indicador	
			},
			subtitle: {	text: '  '	},
		
			xAxis: { categories: arr_anios, 
						title: {text: 'Años'	}
			},
			yAxis: {
					title: { text: 'Valor'	}
			},
			tooltip: {
				enabled: true,
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+
						this.x +': '+ this.y +' '+this.series.name;
				}
			},
			plotOptions: {
				line: {
					dataLabels: {
						enabled: true
					},
					enableMouseTracking: true
				}
			},
			series: [{ name: 'Miles de pesos', data: arr_valor }], //Valores
		});	
}
 
 
 //------ seccion Indicadores--------//
 
 function leer_tema(){
  var desc_tema= document.getElementById("temas").value
  
  for (var llave in obj_temas.tema_1.tema_2.temas_3) {
      var comp_tema=obj_temas.tema_1.tema_2.temas_3[llave].tema_3.desc_tema_3;
      if( comp_tema == desc_tema){
          var id_tema=obj_temas.tema_1.tema_2.temas_3[llave].tema_3.id;
     }
     }
      return id_tema;
}


function solicitar_indicadores(){
    var recurso = "entidades/"+leer_entidad()+"/municipios/"+leer_municipio()+"/temas/"+leer_tema()+"/indicadores";
    solicitar(recurso);
}


function inicia_indicadores(){
    
    solicitar_indicadores();
    if (document.getElementById("indicador")){
         if ( b_indicador.style.display == 'none'){
              b_indicador.style.display = 'inline';
              hr_indicador.style.display = 'none';
              p_indicador.style.display = 'none';
              sct_indicador.style.display = 'none';
    }
         return 0;
     };

b_indicador= crear_boton(boton_atributos,"indicador","Indicadores");
add_func_boton(b_indicador,get_indicador);
asociar_hijo(b_indicador,div_center_indicador);
}
 
  function get_indicador(){
    load_indicadores(leer_entidad(),leer_municipio(),leer_tema());
}

/*-----------------------------------------------------------------------------------*/  
function load_indicadores(_id_entidad,_id_municicpio,_id_tema){
   var  recurso="entidades/"+_id_entidad+"/municipios/"+_id_municicpio+"/temas/"+_id_tema+"/indicadores";
  console.log(recurso);
  obj_indicador=solicitar(recurso);
 gui_indicadores(obj_indicador,"indicadores");
}
 
function gui_indicadores(obj_indicadores,_id){
arr_indicadores=[];
for (var llave in obj_indicador.indicadores) {
      var temas = obj_indicador.indicadores[llave].indicador;
      arr_indicadores[llave]= temas.desc_indicador;
}

if(arr_indicadores.length == 0){
    b_indicador.style.display = 'none';
    p_indicador=document.createElement("p");
    p_indicador.textContent = obj_temas.mensaje ;
    asociar_hijo(p_indicador,div_center_indicador);
    return 0;
}

 if (document.getElementById("indicador")){
         if ( b_indicador.style.display == 'inline'){
              b_indicador.style.display = 'none';

              p_indicador.removeChild(strong_indicador);
             div_center_indicador.removeChild(p_indicador);
             div_center_indicador.removeChild(hr_indicador);
             div_center_indicador.removeChild(sct_indicador);
    }};
    
    b_indicador.style.display = 'none';
    p_indicador=document.createElement("p");
    strong_indicador=document.createElement("strong");
    strong_indicador.textContent = "Indicadores";
    hr_indicador=document.createElement("hr");
    sct_indicador=crear_select_on(atributos_select,_id,arr_indicadores,"inicia_datos");
    sct_indicador.selectedIndex="-1";
    asociar_hijo(strong_indicador,p_indicador);
    asociar_hijo(p_indicador,div_center_indicador);
    asociar_hijo(hr_indicador,div_center_indicador);
    asociar_hijo(sct_indicador,div_center_indicador);
} 
 
 
 //-------- Seccion TEMAS-------//

function leer_municipio(){
  var desc_municipio= document.getElementById("municipios").value
  
  for (var llave in obj_municipios.municipios) {
      var comp_municipio=obj_municipios.municipios[llave].municipio.desc_municipio;
      if( comp_municipio == desc_municipio){
          var id_muncicpio=obj_municipios.municipios[llave].municipio.id;
     }
     }
      return id_muncicpio;
}

function solicitar_temas(){
    var recurso = "entidades/"+leer_entidad()+"/municipios/"+leer_municipio()+"/temas";
    solicitar(recurso);
}

function inicia_temas(){
    solicitar_temas();
    if (document.getElementById("tema")){
         if ( b_tema.style.display == 'none'){
              b_tema.style.display = 'inline';
              hr_tema.style.display = 'none';
              p_tema.style.display = 'none';
              sct_tema.style.display = 'none';
    }
         return 0;
     };

b_tema= crear_boton(boton_atributos,"tema","Temas");
add_func_boton(b_tema,get_temas);
asociar_hijo(b_tema,div_center_tema);
}


 function get_temas(){
    load_temas(leer_entidad(),leer_municipio());
}
/*-----------------------------------------------------------------------------------*/  
function load_temas(_id_entidad,_id_municicpio){
   var  recurso="entidades/"+_id_entidad+"/municipios/"+_id_municicpio+"/temas";
  console.log(recurso);
  obj_temas=solicitar(recurso);
  gui_temas(obj_temas,"temas");
}

function gui_temas(obj_temas,_id){
arr_temas=[];
if (obj_temas.tema_1){
for (var llave in obj_temas.tema_1.tema_2.temas_3) {
      var temas = obj_temas.tema_1.tema_2.temas_3[llave].tema_3;
      arr_temas[llave]= temas.desc_tema_3;
}  
};


if(arr_temas.length == 0){
    b_tema.style.display = 'none';
    p_tema=document.createElement("p");
    p_tema.textContent = obj_temas.mensaje ;
    asociar_hijo(p_tema,div_center_tema);
    return 0;
}

 if (document.getElementById("tema")){
         if ( b_tema.style.display == 'inline'){
              b_tema.style.display = 'none';
              
            // p_tema.removeChild(strong_tema);
             div_center_tema.removeChild(p_tema);
             div_center_tema.removeChild(hr_tema);
             div_center_tema.removeChild(sct_tema);
    }};
    
    b_tema.style.display = 'none';
    p_tema=document.createElement("p");
    strong_tema=document.createElement("strong");
    strong_tema.textContent = "Temas";
    hr_tema=document.createElement("hr");
    sct_tema=crear_select_on(atributos_select,_id,arr_temas,"inicia_indicadores");
    sct_tema.selectedIndex="-1";
    asociar_hijo(strong_tema,p_tema);
    asociar_hijo(p_tema,div_center_tema);
    asociar_hijo(hr_tema,div_center_tema);
    asociar_hijo(sct_tema,div_center_tema);
}



//------- seccion MUNICIPIOS-------///
function leer_entidad(){
  var desc_entidad= document.getElementById("entidades").value
  
  for (var llave in obj_entidades.entidades) {
      var comp_entidad=obj_entidades.entidades[llave].entidad.desc_entidad;
      if( comp_entidad == desc_entidad){
          var id_entidad=obj_entidades.entidades[llave].entidad.id;
     }
     }
      return id_entidad;
}

function solicitar_municipios(){
    var recurso = "entidades/"+leer_entidad()+"/municipios";
    solicitar(recurso);
}

 function inicia_municipio(){
solicitar_municipios();
     if (document.getElementById("municipio")){
         if ( b_municipio.style.display == 'none'){
              b_municipio.style.display = 'inline';
              hr_municipio.style.display = 'none';
              p_municipio.style.display = 'none';
              sct_municipio.style.display = 'none';
    }
      if (document.getElementById("tema")){
             b_tema.style.display = 'none';
             p_tema.style.display = 'none';
           hr_tema.style.display = 'none';
           sct_tema.style.display = 'none';
        
     };
     
           if (document.getElementById("indicador")){
             b_indicador.style.display = 'none';
             p_indicador.style.display = 'none';
           hr_indicador.style.display = 'none';
           sct_indicador.style.display = 'none';
           b_graficar.style.display = 'none';
         div_grafica.style.display = 'none';
     };
     
     return 0;
     };


b_municipio= crear_boton(boton_atributos,"municipio","Municipios");
add_func_boton(b_municipio,get_municipios);
asociar_hijo(b_municipio,div_center_municipio);

}

 function get_municipios(){
    load_municipios(leer_entidad());
}

/*-----------------------------------------------------------------------------------*/  
function load_municipios(_id_entidad){
   var  recurso="entidades/"+_id_entidad+"/municipios";
  console.log(recurso);
  obj_municipios=solicitar(recurso);
gui_municipios(obj_municipios,"municipios");
}

/*-----------------------------------------------------------------------------------*/  

function gui_municipios(obj_municipios,_id){
arr_municipios=[];
for (var llave in obj_municipios.municipios) {
      var municipios = obj_municipios.municipios[llave];
      arr_municipios[llave]=municipios.municipio["desc_municipio"];
}

if(arr_municipios.length == 0){
    b_municipio.style.display = 'none';
    p_municipio=document.createElement("p");
    p_municipio.textContent = obj_municipios.mensaje ;
    asociar_hijo(p_municipio,div_center_municipio);
    return 0;
}

 if (document.getElementById("municipio")){
         if ( b_municipio.style.display == 'inline'){
              b_municipio.style.display = 'none';

              p_municipio.removeChild(strong_municipio);
             div_center_municipio.removeChild(p_municipio);
             div_center_municipio.removeChild(hr_municipio);
             div_center_municipio.removeChild(sct_municipio);
             
    }};
    
    b_municipio.style.display = 'none';
    p_municipio=document.createElement("p");
    strong_municipio=document.createElement("strong");
    strong_municipio.textContent = "Municipios";
    hr_municipio=document.createElement("hr");
    sct_municipio=crear_select_on(atributos_select,_id,arr_municipios,"inicia_temas");
    sct_municipio.selectedIndex="-1";
    asociar_hijo(strong_municipio,p_municipio);
    asociar_hijo(p_municipio,div_center_municipio);
    asociar_hijo(hr_municipio,div_center_municipio);
    asociar_hijo(sct_municipio,div_center_municipio);
}



/*---- Seccion Entidades-------------------------------*/  

function get_entidades(){
  var  recurso="entidades";
  obj_entidades=solicitar(recurso);
    gui_entidades(obj_entidades,recurso);
}
/*-----------------------------------------------------------------------------------*/  
function gui_entidades(obj_entidades,_id){
arr_entidades=[];
for (var llave in obj_entidades.entidades) {
      var entidades = obj_entidades.entidades[llave];
      arr_entidades[llave]=entidades.entidad["desc_entidad"];
}
if(arr_entidades.length == 0){
    b_entidad.style.display = 'none';
    p_entidad=document.createElement("p");
    p_entidad.textContent = obj_entidades.mensaje ;
    asociar_hijo(p_entidad,div_center_entidad);
    return 0;
}
    b_entidad.style.display = 'none';
    p_entidad=document.createElement("p");
    strong_entidad=document.createElement("strong");
    strong_entidad.textContent = "Entidades";
    hr_entidad=document.createElement("hr");
    sct_entidad=crear_select_on(atributos_select,_id,arr_entidades,"inicia_municipio");
    sct_entidad.selectedIndex="-1";
     //add_func_boton(sct_entidad,inicia_municipio);
    asociar_hijo(strong_entidad,p_entidad);
    asociar_hijo(p_entidad,div_center_entidad);
    asociar_hijo(hr_entidad,div_center_entidad);
    asociar_hijo(sct_entidad,div_center_entidad);
}
/*-----Interfaz inicial de la aplicaion entidades-------------------------------*/  
var respuesta="{ }";
var div_container = crear_div(atributos_container);
var div_row = crear_div(atributos_row);
var div_espacio = crear_div(atributos_esp);
var div_entidad = crear_div(atributos_entidad);
var div_center_entidad=crear_div(atributos_pcentrado);
var b_entidad= crear_boton(boton_atributos,"entidad","Entidades");
add_func_boton(b_entidad,get_entidades);


asociar_hijo(div_espacio,div_row);
asociar_hijo(div_entidad,div_row);
asociar_hijo(div_center_entidad,div_entidad);
asociar_hijo(b_entidad,div_center_entidad);
asociar_hijo(div_row,div_container);
asociar_hijo(div_container,document.body);

/*------------ interfaz inicial de municicpios----------------------------------*/  
var div_municipio = crear_div(atributos_municipio);
var div_center_municipio=crear_div(atributos_pcentrado);

asociar_hijo(div_center_municipio,div_municipio);
asociar_hijo(div_municipio,div_row);

/*------------ interfaz inicial de temas----------------------------------*/  
var div_tema = crear_div(atributos_tema);
var div_center_tema=crear_div(atributos_pcentrado);

asociar_hijo(div_center_tema,div_tema);
asociar_hijo(div_tema,div_row);

/*------------ interfaz inicial de indicadores ----------------------------------*/  

var div_indicador = crear_div(atributos_indicador);
var div_center_indicador=crear_div(atributos_pcentrado);

asociar_hijo(div_center_indicador,div_indicador);
asociar_hijo(div_indicador,div_row);

  
var div_container_g = crear_div(atributos_container);
var div_row_grafica=crear_div(atributos_row);
var div_boton =crear_div(atributos_pcentrado);
var div_grafica=crear_div(atributos_grafica);
asociar_hijo(document.createElement("hr"),document.body);
asociar_hijo(div_boton,div_row_grafica);
asociar_hijo(div_row_grafica,div_container_g);
asociar_hijo(div_grafica,div_container_g);
asociar_hijo(div_container_g,document.body);


solicitar("entidades");


 
